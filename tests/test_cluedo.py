import unittest
from cluedo import ALL_ITEMS
from cluedo import PLAYERS
from cluedo import Pad
from cluedo import ValidationException
from cluedo import CARDS_PER_PLAYER
from cluedo import SUSPECTS
from cluedo import WEAPONS
from cluedo import ROOMS


class TestCludeo(unittest.TestCase):

    def setUp(self):
        self.pad = Pad()

    def test_init(self):
        for item in self.pad.pad:
            self.assertIsNotNone(self.pad.pad[item], 'all items inited')

    def test___str__(self):
        print(self.pad)

    def test_count_column(self):
        for item in self.pad.pad:
            self.pad.pad[item]['x'] = True
        self.pad.pad[ALL_ITEMS[-1]]['x'] = None
        self.pad.pad[ALL_ITEMS[-2]]['x'] = None
        self.pad.pad[ALL_ITEMS[0]]['x'] = False

        (t, f, n)  = self.pad.count_column('x')
        self.assertEqual(t, len(ALL_ITEMS)-3)
        self.assertEqual(f, 1)
        self.assertEqual(n, 2)

    def test_count_row(self):
        item = ALL_ITEMS[0]
        self.pad.pad[item][PLAYERS[0]] = True
        self.pad.pad[item][PLAYERS[1]] = False
        self.pad.pad[item][PLAYERS[2]] = False

        (t, f, n) = self.pad.count_row(item)
        self.assertEqual(t, 1)
        self.assertEqual(f, 2)
        self.assertEqual(n, len(PLAYERS)-3)

    def test_set_card_ok(self):
        player = PLAYERS[0]
        item = ALL_ITEMS[0]
        for i in range(1, CARDS_PER_PLAYER[player]):
            self.pad.pad[ALL_ITEMS[i]][player] = True

        self.pad.set_card(item, player)

        for p in PLAYERS:
            card = self.pad.pad[item].get(p)
            if p == player:
                self.assertTrue(card)
            else:
                self.assertEquals(card, False)

        for i in self.pad.pad:
            card = self.pad.pad[i].get(player)
            self.assertNotEqual(card, None)

    def test_set_card_fail(self):
        self.pad.pad[ALL_ITEMS[0]][PLAYERS[1]] = True

        self.assertRaises(ValidationException, lambda: self.pad.set_card(ALL_ITEMS[0], PLAYERS[0]))

    def test_set_card_fail2(self):
        self.pad.pad[ALL_ITEMS[0]][PLAYERS[0]] = False

        self.assertRaises(ValidationException, lambda: self.pad.set_card(ALL_ITEMS[0], PLAYERS[0]))

    def test_infer_row(self):
        item = ALL_ITEMS[0]
        for player in PLAYERS:
            self.pad.pad[item][player] = False
        self.pad.pad[item][PLAYERS[0]] = None

        self.pad.infer()

        self.assertTrue(self.pad.pad[item][PLAYERS[0]])

    def test_infer_column(self):
        player = PLAYERS[0]
        max_false_cnt = len(ALL_ITEMS) - CARDS_PER_PLAYER[player]
        for i in range(max_false_cnt):
            self.pad.pad[ALL_ITEMS[i]][player] = False

        self.pad.infer()

        for i in range(max_false_cnt, len(ALL_ITEMS)):
            self.assertTrue(self.pad.pad[ALL_ITEMS[i]][player])

    def test_infer_solution(self):
        solution = PLAYERS[-1]
        self.pad.pad[SUSPECTS[0]][solution] = True

        self.pad.infer()

        for item in SUSPECTS[1:]:
            self.assertEqual(self.pad.pad[item].get(solution), False)

        for item in WEAPONS:
            self.assertEqual(self.pad.pad[item].get(solution), None)

        for item in ROOMS:
            self.assertEqual(self.pad.pad[item].get(solution), None)

    def test_infer_solution2(self):
        solution = PLAYERS[-1]
        for item in SUSPECTS[1:]:
            self.pad.pad[item][solution] = False

        self.pad.infer()

        self.assertTrue(self.pad.pad[SUSPECTS[0]][solution])

    def test_process_round_err(self):
        round = {'player': 'Scott', 'suspect': 'Wrong', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': None}

        self.assertRaises(Exception, lambda: self.pad.process_round(round))

    def test_process_round_card(self):
        round = {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': 'Green'}

        self.pad.process_round(round)

        self.assertTrue(self.pad.pad['Green']['Jim'])

    def test_process_round(self):
        round = {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': None}

        self.pad.updated = False
        self.pad.process_round(round)

        self.assertTrue(self.pad.updated)
        self.assertEqual(self.pad.pad['Green'].get('Scott'), None)
        self.assertEqual(self.pad.pad['Green'].get(PLAYERS[-1]), None)
        self.assertEqual(self.pad.pad['Green'].get('Jim'), None)
        self.assertEqual(self.pad.pad['Green'].get('Traianus'), False)
        self.assertEqual(self.pad.pad['Green'].get('Jack'), False)

    def test_process_round_wrong_prover(self):
        round = {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': None}
        self.pad.pad['Spanner']['Jack'] = True

        self.assertRaises(Exception, lambda: self.pad.process_round(round))

    def test_process_round_prover_have_no_card(self):
        round = {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': None}
        self.pad.pad['Green']['Jim'] = False
        self.pad.pad['Spanner']['Jim'] = False
        self.pad.pad['Study']['Jim'] = False

        self.assertRaises(Exception, lambda: self.pad.process_round(round))

    def test_process_round_prover_rule_out(self):
        round = {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': None}
        self.pad.pad['Green']['Jim'] = False
        self.pad.pad['Spanner']['Jim'] = False
        self.pad.pad['Study']['Jim'] = None

        self.pad.process_round(round)

        self.assertTrue(self.pad.pad['Study']['Jim'])

    def test_first_unsolved_item(self):
        self.pad.set_card(ALL_ITEMS[0], PLAYERS[0])

        item = self.pad.first_unsolved_item()

        self.assertEqual(item, ALL_ITEMS[1])

    def test_copy(self):
        self.pad.pad[ALL_ITEMS[0]][PLAYERS[0]] = True

        newpad = self.pad.copy()

        self.assertTrue(newpad.pad[ALL_ITEMS[0]].get(PLAYERS[0]))

        self.pad.pad[ALL_ITEMS[0]][PLAYERS[0]] = False
        self.assertTrue(newpad.pad[ALL_ITEMS[0]].get(PLAYERS[0]))

    def test_get_solution(self):
        self.pad.pad[SUSPECTS[0]][PLAYERS[-1]] = True
        self.pad.pad[WEAPONS[0]][PLAYERS[-1]] = True
        self.pad.pad[ROOMS[0]][PLAYERS[-1]] = True

        suspect, weapon, room = self.pad.get_solution()

        self.assertEqual(suspect, SUSPECTS[0])
        self.assertEqual(weapon, WEAPONS[0])
        self.assertEqual(room, ROOMS[0])

    def test_infer_solutions(self):
        self.pad.infer_solutions([(SUSPECTS[0], WEAPONS[0], ROOMS[0]), (SUSPECTS[1], WEAPONS[1], ROOMS[1])])

        for cat in (SUSPECTS, WEAPONS, ROOMS):
            self.assertEqual(self.pad.pad[cat[0]].get(PLAYERS[-1]), None)
            self.assertEqual(self.pad.pad[cat[1]].get(PLAYERS[-1]), None)

            for item in cat[2:]:
                self.assertEqual(self.pad.pad[item].get(PLAYERS[-1]), False)
        self.assertTrue(self.pad.updated)

    def test_count_none(self):
        cnt = self.pad.count_none()

        self.assertEqual(cnt, len(PLAYERS)*len(ALL_ITEMS))

        self.pad.pad[ALL_ITEMS[0]][PLAYERS[0]] = False
        cnt2 = self.pad.count_none()

        self.assertEqual(cnt2, cnt-1)