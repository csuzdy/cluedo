import collections
import copy
import cProfile
import time
import itertools
import statistics

SUSPECTS = ['Mustard', 'Plum', 'Green', 'Peacock', 'Scarlet', 'White']
WEAPONS = ['Rope', 'Dagger', 'Spanner', 'Revolver', 'LeadPipe', 'CandleStick']
ROOMS = ['Kitchen', 'Ballroom', 'Coservatory', 'DiningRoom', 'BilliardRoom', 'Library', 'Study', 'Hall', 'Lounge']
ALL_ITEMS = SUSPECTS + WEAPONS + ROOMS
ALL_ITEMS_CNT = len(ALL_ITEMS)

PLAYERS = ['Traianus', 'Jack', 'Jim', 'Scott', 'Solution']
DOUBLE_PLAYERS = PLAYERS + PLAYERS

PLAYER_CNT = len(PLAYERS)
CARDS_PER_PLAYER = {'Traianus': 4, 'Jack': 5, 'Jim': 5, 'Scott': 4, 'Solution': 3}
KNOWN = ['Revolver', 'Kitchen', 'Ballroom', 'Lounge']

ROUNDS = [
    {'player': 'Traianus', 'suspect': 'White', 'weapon': 'Rope', 'room': 'Lounge', 'prover': 'Jack', 'card': 'Rope'},
    {'player': 'Jack', 'suspect': 'White', 'weapon': 'CandleStick', 'room': 'DiningRoom', 'prover': 'Jim', 'card': None},
    {'player': 'Traianus', 'suspect': 'White', 'weapon': 'Rope', 'room': 'Hall', 'prover': 'Jack', 'card': 'Rope'},
    {'player': 'Jim', 'suspect': 'Mustard', 'weapon': 'Revolver', 'room': 'Coservatory', 'prover': 'Traianus', 'card': 'Revolver'},
    {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Revolver', 'room': 'Study', 'prover': 'Traianus', 'card': 'Revolver'},
    {'player': 'Traianus', 'suspect': 'White', 'weapon': 'Revolver', 'room': 'Ballroom', 'prover': 'Jim', 'card': 'White'},
    {'player': 'Jim', 'suspect': 'Peacock', 'weapon': 'CandleStick', 'room': 'Lounge', 'prover': 'Scott', 'card': None},
    {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Rope', 'room': 'Kitchen', 'prover': 'Traianus', 'card': None},
    {'player': 'Traianus', 'suspect': 'Peacock', 'weapon': 'Revolver', 'room': 'DiningRoom', 'prover': 'Scott', 'card': 'Peacock'},
    {'player': 'Jack', 'suspect': 'Scarlet', 'weapon': 'Revolver', 'room': 'Study', 'prover': 'Scott', 'card': None},
    {'player': 'Jim', 'suspect': 'Mustard', 'weapon': 'Rope', 'room': 'Kitchen', 'prover': 'Traianus', 'card': None},
    {'player': 'Scott', 'suspect': 'Plum', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': None},
    {'player': 'Traianus', 'suspect': 'Plum', 'weapon': 'Revolver', 'room': 'Study', 'prover': 'Jim', 'card': 'Plum'},
    {'player': 'Jack', 'suspect': 'Mustard', 'weapon': 'Dagger', 'room': 'Kitchen', 'prover': 'Traianus', 'card': None},
    {'player': 'Jim', 'suspect': 'Scarlet', 'weapon': 'Dagger', 'room': 'Study', 'prover': 'Scott', 'card': None},
    {'player': 'Scott', 'suspect': 'Green', 'weapon': 'Dagger', 'room': 'Hall', 'prover': 'Jack', 'card': None},
    {'player': 'Traianus', 'suspect': 'White', 'weapon': 'Dagger', 'room': 'Study', 'prover': 'Jim', 'card': 'White'},
    {'player': 'Scott', 'suspect': 'White', 'weapon': 'Dagger', 'room': 'Study', 'prover': 'Jim', 'card': None},
    {'player': 'Traianus', 'suspect': 'Mustard', 'weapon': 'Dagger', 'room': 'Hall', 'prover': 'Jack', 'card': 'Hall'},
    {'player': 'Jack', 'suspect': 'Peacock', 'weapon': 'Spanner', 'room': 'Study', 'prover': 'Jim', 'card': None},
    {'player': 'Scott', 'suspect': 'Plum', 'weapon': 'LeadPipe', 'room': 'Library', 'prover': 'Jack', 'card': None},
    {'player': 'Traianus', 'suspect': 'Mustard', 'weapon': 'Revolver', 'room': 'Kitchen', 'prover': None, 'card': None},
    {'player': 'Traianus', 'suspect': 'Green', 'weapon': 'Revolver', 'room': 'Library', 'prover': 'Jim', 'card': 'Library'},
    {'player': 'Jack', 'suspect': 'Plum', 'weapon': 'Dagger', 'room': 'Library', 'prover': 'Jim', 'card': None},
    {'player': 'Jim', 'suspect': 'Mustard', 'weapon': 'Dagger', 'room': 'Hall', 'prover': 'Jack', 'card': None},
    {'player': 'Scott', 'suspect': 'Plum', 'weapon': 'Rope', 'room': 'Study', 'prover': 'Jack', 'card': None},
    {'player': 'Traianus', 'suspect': 'Mustard', 'weapon': 'Revolver', 'room': 'Study', 'prover': None, 'card': None}
]
solutions = set()


class ValidationException(Exception):

    def __init__(self, msg):
        super().__init__(msg)


class Pad:

    def __init__(self):
        super().__init__()
        self.pad = {}
        self.updated = None
        for item in ALL_ITEMS:
            self.pad[item] = {}

    def __str__(self):
        ret = ''
        for item in ALL_ITEMS:
            ret += '%12s\t' % item
            for player in PLAYERS:
                if self.pad[item].get(player) is None:
                    ret += 'O '
                elif self.pad[item][player]:
                    ret += 'X '
                else:
                    ret += '- '
            ret += '\n'
        return ret

    def __eq__(self, other):
        return self.pad == other.pad

    def __hash__(self):
        return hash(self.pad)

    def copy(self):
        newpad = Pad()
        newpad.pad = copy.deepcopy(self.pad)
        return newpad

    def count_column(self, player, items = ALL_ITEMS):
        """Returns count of True, False and None fields of items of the player"""
        t = f = n = 0
        for item in items:
            card = self.pad[item].get(player)
            if card is None:
                n += 1
            elif card:
                t += 1
            else:
                f += 1
        return t, f, n

    def count_row(self, item):
        """Returns count of True, False and None fields of card of the item"""
        t = f = n = 0
        for player in PLAYERS:
            card = self.pad[item].get(player)
            if card is None:
                n += 1
            elif card:
                t += 1
            else:
                f += 1
        return t, f, n

    def set_card(self, item, player):
        for p in PLAYERS:
            card = self.pad[item].get(p)
            if player == p:
                require_card(card, [None, True])
                self.pad[item][p] = True
            else:
                require_card(card, [None, False])
                self.pad[item][p] = False

        t, f, n = self.count_column(player)
        max_cards_of_player = CARDS_PER_PLAYER[player]
        if t > max_cards_of_player:
            raise ValidationException('Too many cards for player: %s' % player)
        elif t == max_cards_of_player:
            for i in self.pad:
                card = self.pad[i].get(player)
                if card is None:
                    self.pad[i][player] = False

    def infer(self):
        """Infer cards based on implicit rules"""
        self.updated = True
        while self.updated:
            self.updated = False
            self._infer_row()
            self._infer_column()
            self._infer_solution()

    def _infer_row(self):
        """If there is exectly 1 None in a row and the others are False, None should be True"""
        for item in self.pad:
            t, f, n = self.count_row(item)
            if n == 1 and f == PLAYER_CNT-1:
                player = [player for player in PLAYERS if self.pad[item].get(player) is None][0]
                self.set_card(item, player)
                self.updated = True
            if t > 1:
                raise ValidationException('Item belongs to 2 or more player: %s' % item)
            if f == PLAYER_CNT:
                raise ValidationException('%s belongs to nobody' % item)

    def _infer_column(self):
        """Number of cards of a player is limited, if the outhers are ruled out, the others should belong to player"""
        for player in PLAYERS:
            t, f, n = self.count_column(player)
            if t > CARDS_PER_PLAYER[player]:
                raise ValidationException('%s has too many cards' % player)
            max_false_cnt = ALL_ITEMS_CNT - CARDS_PER_PLAYER[player]
            if f > max_false_cnt:
                raise ValidationException('%s has not enough cards' % player)
            if f == max_false_cnt:
                for item in ALL_ITEMS:
                    if self.pad[item].get(player) is None:
                        self.set_card(item, player)
                        self.updated = True

    def _infer_solution(self):
        """There should be 1 solution in all categories (suspects, weapons, rooms)"""
        solution = PLAYERS[-1]
        for items in (SUSPECTS, WEAPONS, ROOMS):
            t, f, n = self.count_column(solution, items)
            if t > 1:
                raise ValidationException('Too many solutions in category: %s' % items)
            if f == len(items):
                raise ValidationException('All solutions ruled out of these: %s' % items)
            if t == 1:
                for item in items:
                    card = self.pad[item].get(solution)
                    if card is None:
                        self.pad[item][solution] = False
                        self.updated = True
            if t == 0 and f == len(items)-1:
                for item in items:
                    card = self.pad[item].get(solution)
                    if card is None:
                        self.set_card(item, solution)
                        self.updated = True

    def process_round(self, round):
        """Process an accusation round"""
        #check_round(round)
        card = round.get('card')
        player = round['player']
        prover = round['prover']
        suspect = round['suspect']
        weapon = round['weapon']
        room = round['room']
        if card:
            self.set_card(card, prover)

        players = DOUBLE_PLAYERS[PLAYERS.index(player)+1:]
        for p in players:
            if p == PLAYERS[-1]:
                continue
            if p == player:
                break
            if p != prover:
                self._process_not_prover(suspect, p)
                self._process_not_prover(weapon, p)
                self._process_not_prover(room, p)
            else:
                self._process_pover(suspect, weapon, room, p)
                break

    def _process_not_prover(self, item, player):
        """If the player did not prove, he mustn't have any of the cards"""
        card = self.pad[item].get(player)
        require_card(card, [None, False])
        if card is None:
            self.pad[item][player] = False
            self.updated = True

    def _process_pover(self, suspect, weapon, room, p):
        """If the player proved, he have to have one of the cards"""
        possibles = []
        if self.pad[suspect].get(p) != False:
            possibles.append(suspect)
        if self.pad[weapon].get(p) != False:
            possibles.append(weapon)
        if self.pad[room].get(p) != False:
            possibles.append(room)

        possibles_cnt = len(possibles)
        if possibles_cnt == 0:
            raise ValidationException('Wrong prover: %s' % p)
        elif possibles_cnt == 1:
            self.set_card(possibles[0], p)
            self.updated = True

    def first_unsolved_item(self):
        """Search for the first item, that does not belong to a player yet"""
        for item in ALL_ITEMS:
            t, f, n = self.count_row(item)
            if t == 0:
                return item
        return None

    def get_solution(self):
        """Returns the items of the Solution virtual player"""
        solution = []
        categories = [SUSPECTS, WEAPONS, ROOMS]
        for cat in categories:
            for item in cat:
                if self.pad[item].get(PLAYERS[-1]):
                    solution.append(item)
                    break
        return tuple(solution)

    def infer_solutions(self, possible_solutions):
        """Rule out possible solutions, that are not in the possible solutions list generated by probe"""
        possible_suspects = {t[0] for t in possible_solutions}
        possible_weapons = {t[1] for t in possible_solutions}
        possible_rooms = {t[2] for t in possible_solutions}
        for cat, possibles in ((SUSPECTS, possible_suspects), (WEAPONS, possible_weapons), (ROOMS, possible_rooms)):
            for p in possibles:
                card = self.pad[p].get(PLAYERS[-1])
                require_card(card, [None, True])
            for item in cat:
                if item not in possibles:
                    card = self.pad[item].get(PLAYERS[-1])
                    require_card(card, [None, False])
                    if card is None:
                        self.pad[item][PLAYERS[-1]] = False
                        self.updated = True

    def count_none(self):
        """Count the Nones of the pad"""
        cnt = 0
        for item in ALL_ITEMS:
            for player in PLAYERS:
                if self.pad[item].get(player) is None:
                    cnt += 1
        return cnt


def require_card(card, value):
    if isinstance(value, collections.Sequence):
        if card not in value:
            raise ValidationException('Required %s, but was: %s' % (value, card))
    elif card != value:
        raise ValidationException('Required %s, but was: %s' % (value, card))


def is_none(val):
    return val is None


def check_round(round):
    """Check round data (misspelling)"""
    categories = [
                    [round['player'], PLAYERS],
                    [round['suspect'], SUSPECTS],
                    [round['weapon'], WEAPONS],
                    [round['room'], ROOMS]
                 ]
    for category in categories:
        if category[0] not in category[1]:
            raise Exception('%s not in %s: ' % (category[0], category[1]))


main_pad = Pad()


def add_known():
    for card in KNOWN:
        main_pad.set_card(card, 'Traianus')


def process_rounds(pad):
    for round in ROUNDS:
        #input()
        pad.updated = True
        while pad.updated:
            pad.updated = False
            pad.process_round(round)
            pad.infer()


def probe(pad, level):
    print('\r' + '#'*level + '       ', end = '')
    item = pad.first_unsolved_item()
    if not item:
        solution = pad.get_solution()
        if solution not in solutions:
            solutions.add(solution)
        return
    for player in PLAYERS:
        if pad.pad[item].get(player) is None:
            #print('Fix: %s, %s' % (item, player))
            newpad = pad.copy()
            try:
                newpad.set_card(item, player)
                process_rounds(newpad)
            except ValidationException:
                continue
            #print(newpad)
            #input()
            probe(newpad, level + 1)


def add_round():
    global main_pad
    cnt = main_pad.count_none()
    temp_pad = main_pad.copy()
    round = read_round()
    temp_pad.updated = True
    while temp_pad.updated:
        temp_pad.updated = False
        temp_pad.process_round(round)
        temp_pad.infer()
    ROUNDS.append(round)
    process_rounds(temp_pad)
    main_pad = temp_pad
    print(main_pad)
    print('Inferred: %d' % (cnt - main_pad.count_none()))


def read_round():
    player = read_card(PLAYERS, 'Player')
    suspect = read_card(SUSPECTS, 'Suspect')
    weapon = read_card(WEAPONS, 'Weapon')
    room = read_card(ROOMS, 'Room')
    prover = read_card(PLAYERS, 'Prover')
    if player == PLAYERS[0]:
        card = read_card(ALL_ITEMS, 'Card')
    else:
        card = None
    return {'player': player, 'suspect': suspect, 'weapon': weapon, 'room': room, 'prover': prover, 'card': card}


def read_card(category, name):
    for i, item in enumerate(category):
        print('%d. %s' % (i, item))
    idx_str = input('%s: ' % name)
    print()
    if idx_str:
        idx = int(idx_str)
        return category[idx]
    else:
        return None


def run_probe():
    solutions.clear()
    start = time.time()
    cnt = main_pad.count_none()
    probe(main_pad, 1)
    print('Elapsed time: %d sec' % (time.time() - start))
    print('Solution count: ', len(solutions))
    print('Solutions: ')
    res = sorted(list(solutions))
    main_pad.updated = False
    main_pad.infer_solutions(res)
    if main_pad.updated:
        process_rounds(main_pad)
        run_probe()
    for sol in res:
        print(sol)
    print('Inferred: %d' % (cnt - main_pad.count_none()))


def advice_suggestion(pad):
    suggestions = []
    start = time.time()
    for s, w, r in itertools.product(SUSPECTS, WEAPONS, ROOMS):
        counts = infer_counts(pad, s, w, r)
        mean = statistics.mean(counts)
        suggestions.append((mean, (s, w, r)))
    suggestions.sort(key=lambda x: x[0])
    print('Suggestion advices:')
    for suggestion in suggestions:
        print(suggestion)
    print('Elapsed time: %d sec' % (time.time() - start))


def infer_counts(pad, suspect, weapon, room):
    players = ([], [], [])
    counts = []
    for player in PLAYERS:
        for i, cat in enumerate((suspect, weapon, room)):
            if pad.pad[cat].get(player) in [None, True]:
                players[i].append(player)

    for sp, wp, rp in itertools.product(players[0], players[1], players[2]):
        newpad = pad.copy()
        before = newpad.count_none()
        try:
            newpad.set_card(suspect, sp)
            newpad.set_card(weapon, wp)
            newpad.set_card(room, rp)
            newpad.infer()
            process_rounds(newpad)
            counts.append(before - newpad.count_none())
        except ValidationException:
            pass
    return counts

if __name__ == '__main__':
    add_known()
    #ROUNDS = []
    process_rounds(main_pad)
    #cProfile.run('probe(main_pad)')
    print(main_pad)
