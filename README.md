A tiny python program, that helps to infer cards of other players in Cluedo board game. 
See more information about Cluedo on [Wikipedia](https://en.wikipedia.org/wiki/Cluedo)

## Run it with Docker
```docker
docker build -t csuzdy:cluedo .
docker run --rm csuzdy:cluedo
```
